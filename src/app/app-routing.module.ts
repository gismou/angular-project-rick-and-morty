import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomePageComponent } from './pages/home-page/home-page.component';
import { CharactersComponent } from './pages/characters/characters.component';
import { DetailComponent } from './pages/characters/detail/detail.component';
import { ContactComponent } from './pages/contact/contact.component';
import { FavouritesComponent } from './pages/favourites/favourites.component';
import { LocationsComponent } from './pages/locations/locations.component';
import { DetailLocationComponent } from './pages/locations/detail-location/detail-location.component';


const routes: Routes = [
  {path: "", component: HomePageComponent},
  {path: "characters", component: CharactersComponent},
  {path: "characters/:idCharacter", component: DetailComponent},
  {path: "contact", component: ContactComponent},
  {path: "favourites", component: FavouritesComponent},
  {path: "locations", component: LocationsComponent},
  {path: "locations/:idLocation", component: DetailLocationComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
