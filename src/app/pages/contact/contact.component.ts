import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss']
})
export class ContactComponent implements OnInit {

  contactForm;
  submitted:boolean = false;

  constructor(private formBuilder : FormBuilder) { 
    this.contactForm = this.formBuilder.group({
      name: ['',[Validators.required, Validators.minLength(4)]],
      email: ['',[Validators.required, Validators.email]],
      message: ['',[Validators.required, Validators.minLength(10)]],
      check: [false,[Validators.requiredTrue  ]]
    })
  }

  ngOnInit(): void {
  }

  submitForm(){
    this.submitted = true;
    console.log(this.contactForm);
    console.log(this.contactForm.value);
  }

}
