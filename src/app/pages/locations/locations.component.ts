import { LocationsService } from './../../shared/services/locations.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-locations',
  templateUrl: './locations.component.html',
  styleUrls: ['./locations.component.scss']
})
export class LocationsComponent implements OnInit {

  locations;

  search;

  constructor(private router:Router, private locationsService : LocationsService) { }

  searchLocation(){
    this.locationsService.getLocations(this.search).subscribe( (data:any) =>{
      this.locations = data.results;
      console.log(this.locations);
    })
  }

  openDetail(id){
    this.router.navigateByUrl("/locations/"+id)
  }

  ngOnInit(): void {
    this.searchLocation();
  }

}
