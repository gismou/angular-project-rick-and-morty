import { LocationsService } from 'src/app/shared/services/locations.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-detail-location',
  templateUrl: './detail-location.component.html',
  styleUrls: ['./detail-location.component.scss']
})
export class DetailLocationComponent implements OnInit {

  location;

  constructor(private route : ActivatedRoute, private locationsService:LocationsService) { }


  ngOnInit(): void {
    this.route.paramMap.subscribe(params => {
      const id = params.get('idLocation');
      this.locationsService.getLocation(id).subscribe( (findedLocation:any) => {
        this.location = findedLocation;
        console.log(this.location);
      })
    })
  }

}
