import { LocationsService } from './../../shared/services/locations.service';
import { CharactersService } from './../../shared/services/characters.service';
import { FavouritesService } from './../../shared/services/local/favourites.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-favourites',
  templateUrl: './favourites.component.html',
  styleUrls: ['./favourites.component.scss']
})
export class FavouritesComponent implements OnInit {

  constructor(private favouritesService:FavouritesService, private charactersService:CharactersService, private locationsService:LocationsService) { }

  favourites = [];

  search = "";

  serchAll(){
    
  }

  ngOnInit(): void {
    this.favourites = this.favouritesService.getFavourites();
  }

}
