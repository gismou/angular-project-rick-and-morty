import { CharactersService } from './../../../shared/services/characters.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss']
})
export class DetailComponent implements OnInit {

  character;

  constructor(private route : ActivatedRoute, private characterService:CharactersService) { }

  ngOnInit(): void {
    this.route.paramMap.subscribe(params => {
      const id = params.get('idCharacter');
      this.characterService.getCharacter(id).subscribe( (findedCharacter:any) => {
        this.character = findedCharacter;
        console.log(this.character);
      })
    })
  }

}
