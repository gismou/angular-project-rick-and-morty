import { CharactersService } from './../../shared/services/characters.service';
import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router'

@Component({
  selector: 'app-characters',
  templateUrl: './characters.component.html',
  styleUrls: ['./characters.component.scss']
})
export class CharactersComponent implements OnInit {

  characters;

  search = "";

  constructor(private router:Router, private characterService : CharactersService) { }

  serchCharacter(){
    this.characterService.getCharacters(this.search).subscribe( (data:any) => {
      this.characters = data.results;
      console.log(this.characters);
    })
  }

  openDetail(id){
    this.router.navigateByUrl("/characters/"+id)
  }

  ngOnInit(): void {
    this.serchCharacter();
  } 

}
