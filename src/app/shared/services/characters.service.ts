import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CharactersService {

  constructor(private http: HttpClient) { }

  getCharacters(name = ""){
    return this.http.get('https://rickandmortyapi.com/api/character?name='+name);
  }

  getCharacter(id){
    return this.http.get('https://rickandmortyapi.com/api/character/'+id);
  }

}
