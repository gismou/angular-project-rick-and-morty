import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LocationsService {

  constructor(private http: HttpClient) { }

  getLocations(location = ""){
    return this.http.get('https://rickandmortyapi.com/api/location?name='+location);
  }

  getLocation(id = ""){
    return this.http.get('https://rickandmortyapi.com/api/location/'+id);
  }
}
