import { CharactersService } from './../../services/characters.service';
import { FavouritesService } from './../../services/local/favourites.service';
import { Component, Input, Output, OnInit, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-gallery',
  templateUrl: './gallery.component.html',
  styleUrls: ['./gallery.component.scss']
})
export class GalleryComponent implements OnInit {

  @Input() data;

  @Output() clickEmitter = new EventEmitter();

  favourites = [];

  constructor(private favouritesService:FavouritesService, private charactersService:CharactersService) { }

  ngOnInit(): void {
    this.favourites = this.favouritesService.getFavourites();
    console.log(this.favourites);
  }

  emitId(id){
    this.clickEmitter.emit(id);
  }

}
